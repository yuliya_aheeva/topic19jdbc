<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>order</title>
</head>
<body>
    <div align="center">
        <h1 style="margin-top: 250px">Dear <%= session.getAttribute("name") %>, your order: </h1><br>
        <section>
            <p><%= session.getAttribute("basket") %></p>
        </section>
        <p>Total: $ <%= session.getAttribute("total") %></p><br>
    </div>
</body>
</html>
