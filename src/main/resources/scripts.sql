drop table if exists orders_goods;
drop table if exists goods;
drop table if exists orders;
drop table if exists users;

CREATE TABLE users (
    id          BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user_name    VARCHAR(255),
    password    VARCHAR(255)
);

CREATE TABLE orders (
    id          BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user_id     BIGINT,
    total_price DOUBLE,
    CONSTRAINT fk_orders_users FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE goods (
    id          BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title       VARCHAR(255),
    price       DOUBLE
);

CREATE TABLE orders_goods (
    id          BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    order_id    INT,
    good_id     INT,
    CONSTRAINT fk_orders_goods_order FOREIGN KEY (order_id) REFERENCES orders (id),
    CONSTRAINT fk_orders_goods_good FOREIGN KEY (good_id) REFERENCES goods (id)
);

INSERT INTO users (user_name, password)
VALUES
       ('Ivan', 'ivan1'),
       ('Petr', 'petr1'),
       ('Nikita', 'nikita1'),
       ('Alex', 'alex');

INSERT INTO goods (title, price)
VALUES
        ('Book', 5.5),
        ('Pen', 0.8),
        ('Newspaper', 1.0),
        ('Ruler', 1.2);

select * from users
where user_name='Ivan';