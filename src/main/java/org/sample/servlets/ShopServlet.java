package org.sample.servlets;

import org.sample.entities.Item;
import org.sample.entities.User;
import org.sample.repositories.ShopRepository;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "shopServlet", urlPatterns = "/shop")
public class ShopServlet extends HttpServlet {
  private final HashMap<Item, Integer> basket = new HashMap<>();
  private final ArrayList<Item> list = new ArrayList<>();
  private ShopRepository shopRepository;

  public void init() {
    System.out.println("in init");
    list.add(new Item("Book", 5.5));
    list.add(new Item("Pen", 0.8));
    list.add(new Item("Newspaper", 1.0));
    list.add(new Item("Ruler", 1.2));

    try {
      Class.forName("org.h2.Driver");
      Connection connection = DriverManager.getConnection(
              "jdbc:h2:~/testdb;INIT=RUNSCRIPT FROM 'src/main/resources/scripts.sql'", "sa", "");
      System.out.println("db created");
      shopRepository = new ShopRepository(connection);
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {

    HttpSession session = req.getSession();
    Map<String, String[]> params = req.getParameterMap();
    if (params.isEmpty() || !params.containsKey("terms")) {
      RequestDispatcher dispatcher = req.getRequestDispatcher("WEB-INF/error.jsp");
      dispatcher.forward(req, resp);
    } else {
      String name = params.get("name")[0];
      session.setAttribute("name", name);
      User user = shopRepository.getUserByName(name);
      if (user == null) {
        shopRepository.addUser(session);
      } else {
        session.setAttribute("userId", user.getId());
      }
      RequestDispatcher dispatcher = req.getRequestDispatcher("WEB-INF/page2.jsp");
      dispatcher.forward(req, resp);
    }
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
    String[] title  = req.getParameterValues("goods");
    HttpSession session = req.getSession();
    if (session.getAttribute("orderId") == null) {
      shopRepository.createOrder(session);
    }
    if (req.getParameter("add") != null) {
      doGet(req, resp);
      for (String s : title) {
        Item item = shopRepository.getItemByName(s);
        int count = 1;
        if (s.equals("Book")) {
          if (basket.containsKey(list.get(0))) {
            count = basket.get(list.get(0)) + 1;
          }
          basket.put(list.get(0), count);
          shopRepository.addItemToOrder((long) session.getAttribute("orderId"), item.getId());
        }
        if (s.equals("Pen")) {
          if (basket.containsKey(list.get(1))) {
            count = basket.get(list.get(1)) + 1;
          }
          basket.put(list.get(1), count);
          shopRepository.addItemToOrder((long) session.getAttribute("orderId"), item.getId());
        }
        if (s.equals("Newspaper")) {
          if (basket.containsKey(list.get(2))) {
            count = basket.get(list.get(2)) + 1;
          }
          basket.put(list.get(2), count);
          shopRepository.addItemToOrder((long) session.getAttribute("orderId"), item.getId());
        }
        if (s.equals("Ruler")) {
          if (basket.containsKey(list.get(3))) {
            count = basket.get(list.get(3)) + 1;
          }
          basket.put(list.get(3), count);
          shopRepository.addItemToOrder((long) session.getAttribute("orderId"), item.getId());
        }
      }
      req.setAttribute("basket", basket);
      session.setAttribute("basket", basket);
    }

    if (req.getParameter("submit") != null) {
      resp.sendRedirect(req.getContextPath() + "/sum");
      session.setAttribute("basket", basket);
      double total = 0;
      for (Map.Entry<Item, Integer> entry : basket.entrySet()) {
        total += entry.getKey().getPrice() * entry.getValue();
      }
      session.setAttribute("total", total);
      shopRepository.updateOrder(
              (long) session.getAttribute("orderId"), (long) session.getAttribute("userId"), total);
    }
  }
}
